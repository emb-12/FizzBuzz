import static org.junit.Assert.*;

import org.junit.Test;

public class TestFizzBuzz {

	@Test
	public void testFizzReturn() {
		FizzBuzz fizz = new FizzBuzz(); // acteur
		int res = fizz.fizzBuzz(10, 7, 20); // action
		assertEquals(10, res); // assertion
	}
	
	@Test
	public void testBuzzReturn() {
		FizzBuzz buzz = new FizzBuzz(); // acteur
		int res = buzz.fizzBuzz(10, 7, 14); // action
		assertEquals(7, res); // assertion
	}
	
	@Test
	public void testTestReturn() {
		FizzBuzz test = new FizzBuzz(); // acteur
		int res = test.fizzBuzz(10, 7, 12); // action
		assertEquals(12, res); // assertion
	}

}
